/** Core function for loading and reloading Empiria Course Player */

var params = findParams();

function isAutoload(){
	return (typeof params["reload"] != 'undefined'  &&  params["reload"] != null);
}

function findContentState(){
	var contentState;
	
	if (isAutoload()  &&  isWebStorageSupported()  &&  !!localStorage.state) {
		contentState = localStorage.state;
	} else  if (params["state"]){
		contentState = findAndDecodeStateFromUrl(params["state"]);
	}
	if (contentState == null){
		contentState = "";
	}	
	return contentState;
}

function findContentUrl(){
	var contentUrl;
	if (isAutoload()  &&  isWebStorageSupported()  &&  !!localStorage.url) {
		contentUrl = localStorage.url;
	} else if (params["url"]){
		contentUrl = params["url"];
	} 
	if (contentUrl == null){
		contentUrl = "";
	}
	return contentUrl;
}

function findExpirationDate(){
	var expirationDate;
	if (isWebStorageSupported()  &&  !!localStorage.expirationDate) {
		expirationDate = localStorage.expirationDate;
	} else if (params["expirationDate"]){
		expirationDate = params["expirationDate"];
	} 
	if (expirationDate == null){
		expirationDate = "";
	}
	return expirationDate;
}

function findHotspotId(){
	var hotspotID;
	if (isWebStorageSupported()  &&  !!localStorage.hotspotID) {
		hotspotID = localStorage.hotspotID;
	} else if (params["hotspotID"]){
		hotspotID = params["hotspotID"];
	} 
	if (hotspotID == null){
		hotspotID = "";
	}
	return hotspotID;
}

function clearStorage(){
	if (isWebStorageSupported()){
		localStorage.removeItem("state");
		localStorage.removeItem("url");
	}
}