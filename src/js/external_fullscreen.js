function empiriaMediaFullscreenVideoSupported(){
	var isAndroid = navigator.userAgent.toLowerCase().indexOf("android") != -1;
	return isAndroid;
}

function empiriaMediaFullscreenVideoOpen(id, sources, currentTimeMillipercent){
	var message = "id=" + id + "&sources=" + sources + "&timeMillipercent=" + currentTimeMillipercent;
	if(hotspotDataCommunicator != null)
	{
		hotspotDataCommunicator.callAS3Function("fullscreenOpen", message);
	}
	else
	{
		window.location = "bookshelf://flipbook/fullscreenOpen?" + message;	
	}
}

function empiriaMediaFullscreenVideoCheckHash(){
	var contentHash = window.location.hash.substring(1);
	var tokens = contentHash.split("_fullscreenClose=");
	if (tokens.length > 1){
		var tokenWithTail = tokens[1];
		var nextTokenIndex = tokenWithTail.indexOf("&");
		var tokenEndIndex;
		if (nextTokenIndex == -1){
			tokenEndIndex = tokenWithTail.length;
		} else{
			tokenEndIndex = nextTokenIndex;
		}
		var token = tokenWithTail.substring(0, tokenEndIndex);
		var tokenSplitted = token.split(";");
		var id = tokenSplitted[0];
		var time = tokenSplitted[1];
		empiriaMediaFullscreenVideoOnClose(id, time);
	}
}