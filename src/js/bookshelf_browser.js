var cacheTrick = 0;
var publicationLoaded = false;
var contentState = "";
var contentUrl = "";
var expirationDate = "";
var communicationSocket;

function openPlayerWindow(path, publication, state, expiration)
{
	pairedWindow = null;
	if(communicationSocket)
	{
		communicationSocket.destroy();
		communicationSocket = null;
	}
	publicationLoaded = false;
	if(publication != undefined && publication.indexOf("url=") == 0)
	{
		publication = publication.substring(4);
	}
	if(publication != undefined && publication != "")
	{
		contentUrl = publication;
	}
	else
	{
		contentUrl = "";
	}
	if(state != undefined && state != "")
	{
		contentState = state;
	}
	else
	{
		contentState = "";
	}
	if (expiration != null && expiration != undefined && expiration != "undefined" )
	{
		expirationDate = expiration;
	}
	else
	{
		expirationDate = "";
	}

	if(path.toLowerCase().indexOf("empiria") != -1 
		|| path.toLowerCase().indexOf("htmlviewer") != -1
		|| path.toLowerCase().indexOf("epubviewer") != -1)
	{
		communicationSocket = new easyXDM.Socket({
			remote: path,
			channel: "communicationFrame",
			onMessage: onMessage
		});
	}
	else
	{
		pairedWindow = window.open(path, "_blank", "", false);
	}	
}

function getContent()
{
	if(!publicationLoaded)
	{
		publicationLoaded = true;
		sendMsg("loadPublication", contentUrl, contentState, expirationDate);
	}
}

function saveState()
{
	sendMsg("save");
}

function updateStateInBookshelf(state)
{
	var bookshelf = document.getElementById("Bookshelf");
	if(bookshelf && bookshelf.updateState)
	{
		bookshelf.updateState(state);
	}
	if(pairedWindow)
	{
		pairedWindow.close();
	}
	else 
	{
		sendMsg("closePlayer");
	}
}

window.onbeforeunload = function () 
{
	var bookshelf = document.getElementById("Bookshelf");
	if(bookshelf && bookshelf.isUserLogged && bookshelf.isUserLogged())
	{
		saveState();
		var message = 'Please wait.';
		if (typeof evt == 'undefined') {
		  evt = window.event;
		}
		if (evt) {
			evt.returnValue = message;
		}
		return message;
	}
}