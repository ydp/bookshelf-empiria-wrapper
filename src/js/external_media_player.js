//android: BOOKSHELF external media player interface
var mediaCallsQue = [];
var mediaCallsEnabled = true;
var mediaCallTimeoutId;
var soundJsExternalCounter = 0;
var independentSounds = {};
var queSendTimeoutId;

function empiriaLoadExtensions()
{
	if(ANDROID_PLATFORM)
	{
		return ['ExternalMediaProcessorExtension'];
	}
	return [];	
}

function empiriaExternalMediaInit(sources, id)
{
	var source;
	var supportedSuffix = ".mp3";
	for (src in sources){
		if (sources[src].toLowerCase().indexOf(supportedSuffix, sources[src].length - supportedSuffix.length) != -1){
			source = sources[src];
			break;
		}
	}
	if (source == null){
		log("No source for id " + id + ". Available sources: " + sources);
	} else {
		sendToBookshelfExternalMedia(id, "INIT", source);
	}
}

function empiriaExternalMediaPlay(id)
{
	sendToBookshelfExternalMedia(id, "PLAY");
}

function empiriaExternalMediaPause(id)
{
	sendToBookshelfExternalMedia(id, "PAUSE");
}

function empiriaExternalMediaSeek(id, timeMillis)
{
	sendToBookshelfExternalMedia(id, "SEEK", timeMillis);
}
//independent
function soundJsExternalRegister(source)
{
	var supportedSuffix = ".mp3";
	if (source.toLowerCase().indexOf(supportedSuffix, source.length - supportedSuffix.length) != -1)
	{
	    var id = "snd"+this.soundJsExternalCounter++;
		for(var existingId in independentSounds)
		{
			if(independentSounds[existingId] == source)
			{
				return existingId;
			}
		}
		independentSounds[id] = source;
		mediaCallsQue.push({id:id, action:"INIT_INDEPENDENT", detail:source});
		return id;
	}	
	else
	{
		log("No source for independent source: " + source + ".");
	}
}

function soundJsExternalInit(src)
{
	return soundJsExternalRegister(src);
}

function soundJsExternalPlay(id)
{
	sendToBookshelfExternalMedia(id, "PLAY");
}

function soundJsExternalPause(id)
{
	sendToBookshelfExternalMedia(id, "PAUSE");
}

function soundJsExternalStop(id)
{
	sendToBookshelfExternalMedia(id, "STOP");
}

function soundJsExternalResume(id)
{
	sendToBookshelfExternalMedia(id, "PLAY");
}

//send to bookshelf
function sendToBookshelfExternalMedia(id, action, detail)
{
	mediaCallsQue.push({id:id, action:action, detail:detail});
	clearTimeout(queSendTimeoutId);
	queSendTimeoutId = setTimeout(executeSendQueToBookshelfExternalMedia, 20);
	//executeSendQueToBookshelfExternalMedia();
}

/*
send que of commands
*/
function executeSendQueToBookshelfExternalMedia()
{
	clearTimeout(queSendTimeoutId);
	if(mediaCallsQue.length > 0 && mediaCallsEnabled)
	{
		mediaCallsEnabled = false;
		var detail = "";

		while( mediaCallsQue.length > 0)
		{
			var actionObj = mediaCallsQue.shift();
			var actionDetail = "id||"+actionObj.id+"|||action||"+actionObj.action;
			if(actionObj.detail == undefined)
			{
				actionDetail += "||||";
			}
			else
			{
				actionDetail += "|||detail||"+actionObj.detail+"||||";
			}
			detail += actionDetail;
		}
		detail = detail.substring(0, detail.length-4);
		toBookshelfData = "flipbook/externalMedia?action=QUE&detail="+detail;
		mediaCallTimeoutId = setTimeout(mediaQueCallTimeoutFunction, 1000);
		if(hotspotDataCommunicator != null)
		{
			hotspotDataCommunicator.callAS3Function("externalMedia", "action=QUE&detail="+detail);
		}
		else
		{
			window.location = toBookshelfData;
		}
	}
}
function mediaQueCallTimeoutFunction()
{
	clearTimeout(mediaCallTimeoutId);
	mediaCallsEnabled = true;
	executeSendQueToBookshelfExternalMedia();
}

//response from bookshelf:
function onExternalMediaResponse(id, action, detail)
{
	clearTimeout(mediaCallTimeoutId);
	log(action + ": " + id);
	switch(action)
	{
		case "ON_QUE":
			var responses = detail.split("||||");
			while(responses.length > 0)
			{
				var response = getParamsFromString(responses.shift());
				onExternalMediaResponse(response.id, response.action, response.detail);
			}
			mediaCallsEnabled = true;
			executeSendQueToBookshelfExternalMedia();
			break;
		case "ON_READY":
			empiriaExternalMediaOnReady(id, {duration:detail});
			break;
		case "ON_PLAY":
			empiriaExternalMediaOnPlay(id);
			break;
		case "ON_PAUSE":
			empiriaExternalMediaOnPause(id);
			break;
		case "ON_STOP":
			empiriaExternalMediaOnStop(id);
			break;
		case "ON_TIME_UPDATE":
			empiriaExternalMediaOnTimeUpdate(id, {state:detail});
			break;
		case "ON_END":
			try
			{
				empiriaExternalMediaOnEnd(id);
			}
			catch(err)
			{
				alert(id+": empiriaExternalMediaOnEnd: "+empiriaExternalMediaOnEnd+"\n"+err);
			}
			break;
		case "ON_INDEPENDENT":
		case "ON_ERROR":
			break;
	}
}
function getParamsFromString(str)
{
	var prmarr = str.split("|||");
	var params = {};
	for ( var i = 0; i < prmarr.length; i++) 
	{
		var tmparr = prmarr[i].split("||");
		if (tmparr.length == 2)
		{
			params[tmparr[0]] = tmparr[1];
		}
		else if (tmparr.length == 1)
		{
			params[tmparr[0]] = null;
		}
	}
	return params;
}

function log(msg)
{	
	try
	{
		console.log("AIR: " + msg);
	}
	catch(error)
	{
	}
}