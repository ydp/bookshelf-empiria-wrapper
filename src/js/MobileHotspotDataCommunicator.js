function MobileHotspotDataCommunicator() {}

MobileHotspotDataCommunicator.prototype.storeHotspotData = function (data) {
    StageWebViewBridge.call('storeHotspotData', null, "", Base64.encode(data));
};

MobileHotspotDataCommunicator.prototype.retrieveHotspotData = function(completeHandler)
{
    MobileHotspotDataCommunicator.retrieveCompleteHandler = completeHandler;
    StageWebViewBridge.call('retrieveHotspotData', null, "");
};

MobileHotspotDataCommunicator.prototype.returnHotspotData = function(data)
{
    MobileHotspotDataCommunicator.retrieveCompleteHandler(Base64.decode(data));
};

MobileHotspotDataCommunicator.prototype.callAS3Function = function(as3FunctionName, as3FunctionArgument)
{
    StageWebViewBridge.call(as3FunctionName, null, as3FunctionArgument);
};

MobileHotspotDataCommunicator.prototype.onStageWebViewBridgeReady = function()
{
    StageWebViewBridge.call("hotspotReady", null);
};

hotspotDataCommunicator = new MobileHotspotDataCommunicator();

StageWebViewBridge.deviceReady(hotspotDataCommunicator.onStageWebViewBridgeReady);