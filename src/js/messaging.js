var childWindow;

function onMessage(message, origin)
{
	var prmarr = message.split("&");
	var params = {};
	for (var i = 0; i < prmarr.length; i++) {
		var tmparr = prmarr[i].split("=");
		if (tmparr.length == 2) {
			params[tmparr[0]] = tmparr[1];
		}
		else if (tmparr.length == 1){
		   params[tmparr[0]] = "";
		}
	}
	var valueArr = unescape(params._value).split("|||");
	onMessageCompleted(params._command, valueArr[0], valueArr[1], valueArr[2]);
}
			
function sendMsg(command, value0, value1, value2)
{
	if (communicationSocket) 
	{
		value0 = value0 == undefined ? "" : value0;
		value1 = value1 == undefined ? "" : value1;
		value2 = value2 == undefined ? "" : value2;
		
		var sent = true;
		var message = "_command=" + command;
		var value = value0 + "|||" + value1 + "|||" + value2;
		var escvalue = escape(value);

		try
		{
			communicationSocket.postMessage(message + "&_value=" + escvalue);
		}
		catch(err)
		{
			sent = false;
		}
	}
}
 
 function onMessageCompleted(command, value0, value1, value2)
 {
 	if(command)
	{
		value0 = value0 == undefined ? "" : value0;
		value1 = value1 == undefined ? "" : value1;
		value2 = value2 == undefined ? "" : value2;
		if(this[command])
		{
			this[command](value0, value1, value2);
		}
		else if(childWindow[command])
		{
			childWindow[command](value0, value1, value2);
		}
	}
 }