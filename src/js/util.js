/** Common utilities */

function base64ToUtf8( str ) {
	// remove padding
	var unpadded = str.replace(/((%3D)|[=])+$/i,'');
	// decode
	return decodeURIComponent(escape(window.atob( unpadded )));
}
function findParams(){
	var prmstr = window.location.search.substr(1);
	var prmarr = prmstr.split ("&");
	var params = {};

	for ( var i = 0; i < prmarr.length; i++) {
		var tmparr = prmarr[i].split("=");
		if (tmparr.length == 2)
			params[tmparr[0]] = tmparr[1];
		else if (tmparr.length == 1)
			params[tmparr[0]] = null;
	}
	return params;
}
function findAndDecodeStateFromUrl(contentStateBase64){
	if (contentStateBase64 != null){
		return base64ToUtf8(contentStateBase64);
	}
	return null;
}
function isWebStorageSupported(){
	if(typeof(Storage)!=="undefined")
	{
		return !!Storage  &&  !!localStorage;
	}
	return false;
}

